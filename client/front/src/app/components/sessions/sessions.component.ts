import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Prediction } from 'src/app/models/prediction';
import { ForcastingService } from 'src/app/services/forcasting.service';


@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {
  predictions : any
  categoryList: any = ['Autumn', 'Winter', 'Spring' , 'Summer']


  prediction = {
    arrival_date: '',
    arrival_date_week_number: '',
    season: '',
    stays_in_weekend_nights:'',
    stays_in_week_nights: '',
    nb_persons: '',
    SC:'',
    BB:'',
    HB:'',
    FB:'',
    num_meals:'',
  
  };
  category: any;
  constructor(private forcastingService: ForcastingService, private toastrService: ToastrService) {}

  ngOnInit() {
   
  }
  changeCategory(e:any) {
     this.category=e.target.value; 
    }

  sendPrediction(): void {
    const data = {

    arrival_date: this.prediction.arrival_date,
    arrival_date_week_number: this.prediction.arrival_date_week_number,
    season: this.prediction.season,
    stays_in_weekend_nights:this.prediction.stays_in_weekend_nights,
    stays_in_week_nights: this.prediction.stays_in_week_nights,
    nb_persons: this.prediction.nb_persons,
    SC:this.prediction.SC,
    BB:this.prediction.BB,
    HB:this.prediction.HB,
    FB:this.prediction.FB,
    num_meals:this.prediction.num_meals

    };
console.log(data)
    this.forcastingService.setPrediction(data)
      .subscribe(
        response => {
          this.predictions = Array.of(response);
          console.log(response)
          this.toastrService.success("predicted successfully", "Forecasting")
        },
        error => {
          
          console.log(error);


        });
  }



}
     
  





