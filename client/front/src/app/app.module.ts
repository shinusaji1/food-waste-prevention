import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ViewComponent } from './components/CRUD/view/view.component';
import { LoginComponent } from './components/login/login.component';
import { CreateComponent } from './components/CRUD/create/create.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SessionsComponent } from './components/sessions/sessions.component';
import { ContributionsComponent } from './components/contributions/contributions.component';
import { CommonModule } from '@angular/common';
import { DataComponent } from './components/data/data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [			
    AppComponent,
      DashboardComponent,
      ViewComponent,
      CreateComponent,
      LoginComponent,
      SidebarComponent,
      SessionsComponent,
      ContributionsComponent,
      DataComponent
   ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
    // NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
