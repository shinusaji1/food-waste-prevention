import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  url: string = "http://localhost:8001"; 
  protected resource = "";

  constructor(public http: HttpClient) {
  }

  getLocalData(){
    return new Promise((resolve, reject) => {
      this.http
    .get("/assets/local_data.csv", {responseType: 'text'}).subscribe(datas => {
      let csvToRowArray = datas.split("\n");
      let columns = csvToRowArray[0].split(",");
      // let columns1 = [ 'arrival date', 'stays in week end', '']
      let results = []
              for (let index = 2; index < 12 - 1; index++) {
              let row = csvToRowArray[index].split(",");
              results.push(row);
            }
            resolve({columns: columns, results: results}); 
    }, err => {
        reject(err)
    })
      
    }) 
  }

  getContributions(){
    return this.http.get(this.url + '/getContributions');
  }


}
