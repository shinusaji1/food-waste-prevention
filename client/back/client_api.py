from fastapi import FastAPI
from client import FoodWasteClient
import flwr as fl
import tensorflow as tf
import pandas as pd
import json
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from sklearn.model_selection import train_test_split
import datetime as dt
from math import ceil


from blockchain_service import *
import os
from predict import Prediction_input_data,format_input_data,load_last_global_model_weights,map_num_meals_to_food_quantity


blockchainService = BlockchainService()
client_id = blockchainService.getAddress()

# define date and time to save weights in directories
today = dt.datetime.today()
session = today.strftime("%d-%m-%Y-%H-%M")


from fastapi.middleware.cors import CORSMiddleware

app=FastAPI()
origins = [
  
    "http://localhost:4201",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
class FlLunch:
    def start(self):
        listen_and_participate("./city_hotel_per_day.csv", "127.0.0.1", 4500)
def load_dataset(path: str):
    """Load the training and test data sets."""
    dataset=pd.read_csv(path, index_col='arrival_date', parse_dates=True)
    X = dataset.loc[:].drop(['breakfast', 'num_meal1', 'num_meal2', 'num_meal3', 'num_meal4'], axis=1)
    y = dataset.loc[:, ['breakfast','num_meal1','num_meal2','num_meal3','num_meal4']]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=0, shuffle=False)
    return X_train, y_train, X_test, y_test

def handle_launch_FL_session(ipaddress,port,model,x_train, y_train, x_test, y_test, client_id):
    """
    handles smart contract's addStrategy event by starting flwr client
    """
    fl.client.start_numpy_client(
        server_address=ipaddress+':'+str(port), 
        client = FoodWasteClient(model, x_train, y_train, x_test, y_test, client_id), 
        grpc_max_message_length = 1024*1024*1024)
    
def load_model():
    # Load and compile Keras model from json config file
    with open('config_model_food.json', 'r') as config_model_food:
        config=config_model_food.read()
    data = json.loads(config)
    
    # get model layers 
    model_architecture = data["model_architecture"]
    model_architecture = json.dumps(model_architecture)

    # Model compiling options
    optimizer = data["compile_options"]["optimizer"]
    loss = data["compile_options"]["loss"]
    metrics = data["compile_options"]["metrics"]

    #compile model 
    model = tf.keras.models.model_from_json(model_architecture)
    model.compile(optimizer=optimizer,loss=loss,metrics=metrics)
    return model

@app.post("/participateFL")
def listen_and_participate(datasetPath:str,ipaddress:str ,port:int):
    """ 
    always wait for the central organisation to launch the FL session,
    and then start the flwr client to patricipate in the session
    """
    
    # Load and compile Keras model
    with open('config_model_food.json', 'r') as config_model_food:
        config=config_model_food.read()
    data = json.loads(config)
    
    model_architecture = data["model_architecture"]
    model_architecture = json.dumps(model_architecture)

    # Model compiling options
    optimizer = data["compile_options"]["optimizer"]
    loss = data["compile_options"]["loss"]
    metrics = data["compile_options"]["metrics"]

    #compile model 
    model = tf.keras.models.model_from_json(model_architecture)
    model.compile(optimizer=optimizer,loss=loss,metrics=metrics)
    # Load the local dataset
    x_train, y_train, x_test, y_test = load_dataset(datasetPath)
    
    ##
    #  event listner on smart contract addStrategy event to be added here
    ##
    handle_launch_FL_session(ipaddress,port,model,x_train, y_train, x_test, y_test, client_id)

@app.get("/getContributions")
def getContributions():
    contributions = blockchainService.getContributions()
    json_compatible_item_data = jsonable_encoder(contributions)
    return JSONResponse(content=json_compatible_item_data)

@app.post("/predictFood")
def predict(input_data: Prediction_input_data):
    """
    use the new data to predict the quantity of each food element needed
    for the the next day using the last global model weights received
    
    returns a dict of (food element,quantity to buy) pairs
    """
    # load the model 
    model=load_model()
    # load the last received global model weights
    model.set_weights(load_last_global_model_weights(model,f'./global-weights'))
    # put the input_data in the format expected by the model 
    data=format_input_data(input_data)
    # use the model to predict num of dishes of each meal in the menu
    predictions=model.predict(data)
    for prediction in predictions[0]:
        prediction=0 if prediction<0 else ceil(prediction)
    # use the predictions to calculate the quantity of each food element to buy  
    food_quantities=map_num_meals_to_food_quantity(predictions[0])
    return food_quantities
