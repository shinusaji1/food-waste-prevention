pragma solidity >=0.5.16;
pragma experimental ABIEncoderV2;

contract federation{

struct Weight{
uint256 data_size;
string file_path;
string file_hash;
}

struct GlobalModel{
string file_path;
string file_hash;
}

struct Strategy{
string alg_name;
uint256 num_round;
uint256 nb_client;
}

mapping(uint256 =>mapping(uint256 => mapping(address => Weight))) weights;
mapping(uint256 => mapping(uint256 => GlobalModel)) models;
mapping(uint256 =>  Strategy) strats;
event addStrategyEvent(string _alg_name, uint256 _nb_client,uint256 _num_round);

function add_weights (uint256 _session_number, uint256 _round_number, uint256 _data_size, string memory _file_path, string memory _file_hash) public {
weights[_session_number][_round_number][msg.sender].data_size=_data_size;
weights[_session_number][_round_number][msg.sender].file_path=_file_path;
weights[_session_number][_round_number][msg.sender].file_hash=_file_hash;
}

function get_weight(uint256 _session_number, uint256 _round_number)  public view returns (uint256, string memory, string memory){
    uint256 datasize = weights[_session_number][_round_number][msg.sender].data_size;
    string memory file_path = weights[_session_number][_round_number][msg.sender].file_path;
    string memory file_hash = weights[_session_number][_round_number][msg.sender].file_hash;
    return (datasize, file_path, file_hash);
    
}



function add_models (uint256 _session_number, uint256 _round_number, string memory _file_path, string memory _file_hash) public {
    
models[_session_number][_round_number].file_path=_file_path;
    
models[_session_number][_round_number].file_hash=_file_hash;
    
}

function get_globalModel(uint256 _session_number, uint256 _round_number) public view returns (string memory, string memory){
    
    return (models[_session_number][_round_number].file_path, models[_session_number][_round_number].file_hash);
}


//here we implemented the Strategy(algorithm) add and get 

function add_Strategy (uint256 _session_number, string memory _alg_name, uint256 _nb_client, uint256  _num_round) public returns (string memory){
    
strats[_session_number].alg_name= _alg_name;
    
strats[_session_number].nb_client=_nb_client;

strats[_session_number].num_round=_num_round;
emit addStrategyEvent(_alg_name, _nb_client, _num_round);
return "Strategy added";
    
}

function get_Strategy (uint256 _session_number) view public returns (string memory, uint256, uint256){
    
    return (strats[_session_number].alg_name, strats[_session_number].nb_client , strats[_session_number].num_round);
    
}


 








}
      