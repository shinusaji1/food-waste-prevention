pragma solidity >=0.5.16;
pragma experimental ABIEncoderV2;


contract Contribution {
    struct work {
        bool finished;
        uint dataSize;
    }
    mapping(address => mapping(uint => work)) dataContributions;
    mapping(address => uint) public dataContributionsCount;
    mapping(address => uint) balances;
    address[] public edge_addresses;
    uint[] public rNos;
    event contribEvent(uint _rNo, address _address);
      function calculate_conribution(uint _rNo, bool  _finished, uint _dataSize)  public 
    {
        require(
            (dataContributions[msg.sender][_rNo].finished == true || dataContributions[msg.sender][_rNo].finished == false)
            ,
            "Edge already contributed."
        );

        if (_finished == true) {
            if(_dataSize > 5){
                dataContributions[msg.sender][_rNo] = work(_finished, _dataSize);
                 edge_addresses.push(msg.sender);
                 rNos.push(_rNo);
                 balances[msg.sender] = balances[msg.sender] + 5;
            }
                
        }
       emit contribEvent(_rNo, msg.sender);
    }  
    function get_edge_addresses() view public returns (address[] memory){
    return edge_addresses;
}
    function get_rNos() view public returns (uint[] memory){
    return rNos;
}
    function get_contribution(address _address_edge, uint _rNo) view public returns (bool, uint, uint){
        return (dataContributions[_address_edge][_rNo].finished, dataContributions[_address_edge][_rNo].dataSize, balances[_address_edge]);
    }

     function get_balance(address _address_edge) public view returns(uint)
     {
       return balances[_address_edge];
    }  

}