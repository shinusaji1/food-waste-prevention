import glob
import os
import tensorflow as tf
import json
import pandas as pd
import numpy as np

from fastapi import FastAPI
from pydantic import BaseModel


class Prediction_input_data(BaseModel):
    # the expected keys and the values of json to be received
    # in the body of a HTTP request
    arrival_date: str
    arrival_date_week_number: int
    season: int
    stays_in_weekend_nights:    int
    stays_in_week_nights: int
    nb_persons: int
    SC:int
    BB:int
    HB:int
    FB:int
    num_meals:int


def load_last_global_model_weights(model,weights_directory): 
    """
    loads the last received weights in the directory
    in which the global model weights are saved
    """
    # files list will contain the paths to the npy files in the directory
    files_list=[]
    # check every file under the root directory to have .npy extension
    for root, dirs, files in os.walk(weights_directory, topdown = False):
        for file in files:
            if file.endswith(".npy"):
                files_list.append(os.path.join(root,file))
    # get the latest file
    latest_weights_file = max(files_list, key=os.path.getmtime)
    # load the weights from the file
    weights=np.load(latest_weights_file,allow_pickle=True)
    return weights

def map_num_meals_to_food_quantity(predictions):
    """
    returns the forecast of the quantity of each food element to buy 
    """
    # initialize the num_meals_list with the predicted number of dishs of each meals in the menu
    num_meals_list=predictions.copy()
    # all_meals_ingredient contains the pairs of (ingredient, quantity_in_one_dish) associated
    # to the meals in the menu (meal1,meal2,meal3,meal4,breakfast)
    all_meals_ingredient={}
    all_meals_ingredient['breakfast_ingredients']={'butter':0.1,'black_coffee':0.05,'orange': 0.5}
    all_meals_ingredient['meal1_ingredients']={'tomato': 0.5,'pepper':0.2,'onion':0.1}
    all_meals_ingredient['meal2_ingredients']={'tomato':0.5,'pepper':0.5}
    all_meals_ingredient['meal3_ingredients']={'letuce':1,'onion':0.3}
    all_meals_ingredient['meal4_ingredients']={'potato':0.4,'pepper':0.4}
    
    # all_ingredients_quantities is dictionary that will contain pairs of (ingredient, total_quantity)
    # for the predicted number of dishes per meal
    all_ingredients_quantities={}
    
    # for each meal in the menu, multiply each ingrendient quantity ( initialised with the quantity in one meal)
    # by the total number of that dishes of that meal to be served
    for num_meal,meal in zip(num_meals_list,all_meals_ingredient):
        for ingredient,quantityPerDish in all_meals_ingredient[meal].items():
            all_ingredients_quantities[ingredient]=all_ingredients_quantities[ingredient]+(quantityPerDish*num_meal) if ingredient in all_ingredients_quantities.keys() else quantityPerDish*num_meal
    return all_ingredients_quantities
    

def format_input_data(input_data:Prediction_input_data):
    """
    creates a dataframe from the input data
    """
    data=pd.DataFrame([[pd.to_datetime(input_data.arrival_date) ,
                              input_data.arrival_date_week_number ,
                              input_data.season , input_data.stays_in_weekend_nights,
                              input_data.stays_in_week_nights , input_data.nb_persons,
                              input_data.SC , input_data.BB , input_data.HB , input_data.FB , input_data.num_meals]],
            columns=['arrival_date','arrival_date_week_number', 'season', 'stays_in_weekend_nights',
           'stays_in_week_nights', 'nb_persons', 'SC', 'BB', 'HB', 'FB','num_meals'])
    data.set_index('arrival_date',inplace=True)
    return data
