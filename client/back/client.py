import flwr as fl
import os
import datetime as dt
import numpy as np
from blockchain_service import *
import hashlib
blockchainService = BlockchainService()

# define date and time to save weights in directories

# Define Flower client
class FoodWasteClient(fl.client.NumPyClient):
    def __init__(self, model , x_train, y_train, x_test, y_test, client_id):
        # init the datasets to be used and the model architecture
        self.model=model
        self.x_train, self.y_train = x_train, y_train
        self.x_test, self.y_test = x_test, y_test
        self.client_id=client_id
    def get_parameters(self):
        return self.model.get_weights()

    def fit(self, parameters, config):
        session = config["session"]
        self.model.set_weights(parameters)
        hist = self.model.fit(self.x_train, self.y_train, epochs=config['epochs'], validation_data=(self.x_test, self.y_test), verbose=config['verbose'], batch_size=config['batch_size'])
        params = self.model.get_weights()

        if not (os.path.exists(f'./Local-weights')):
            os.mkdir(f"./Local-weights")

        if not (os.path.exists(f'./Local-weights/Session-{session}')):
            os.mkdir(f"./Local-weights/Session-{session}")

        # Save training weights in the created directory
        filename = f'./Local-weights/Session-{session}/Round-{config["rnd"]}-training-weights.npy'
        np.save(filename, params)
        with open(filename,"rb") as f:
            bytes = f.read() # read entire file as bytes
            readable_hash = hashlib.sha256(bytes).hexdigest()
            print(readable_hash)
        print("result")
        print(config["session"], config["rnd"], len(self.x_train), filename, readable_hash)
        result = blockchainService.addWeight(config["session"], config["rnd"], len(self.x_train), filename, readable_hash)
        return self.model.get_weights(), len(self.x_train), {"loss": hist.history["loss"][0], "mae": hist.history["mae"][0],  "client_id": self.client_id}

    def evaluate(self, parameters, config):
        self.model.set_weights(parameters)
        session = config["session"]
        # create directory for global weights
        if not (os.path.exists(f'./global-weights')):
            os.mkdir(f"./global-weights")
        if not (os.path.exists(f'./global-weights/Session-{session}')):
            os.mkdir(f"./global-weights/Session-{session}")

        # Save global weights in the created directory
        global_rnd_model = self.model.get_weights()
        np.save(f'./global-weights/Session-{session}/Round-{config["rnd"]}-global-weights.npy', global_rnd_model)

        loss, mae = self.model.evaluate(self.x_test, self.y_test, verbose=config['verbose'])
        print("Eval mae : ", mae)
        return loss, len(self.x_test), {"mae": mae}

