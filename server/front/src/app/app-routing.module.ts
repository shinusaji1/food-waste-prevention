import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { ContributionsComponent } from './components/contributions/contributions.component';
import { SessionsComponent } from './components/sessions/sessions.component';

const routes: Routes = [
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: "dashboard", component: DashboardComponent},
  {path: "contributions", component: ContributionsComponent},
  {path: "sessions", component: SessionsComponent},
  {path: "login", component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
