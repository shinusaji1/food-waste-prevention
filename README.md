<div align="center">

<img src="./images/logo_innov_fact.PNG" >
<p align="center">
<img src="./images/trash.png"height="150" >
</p>

<h2 align="center">Food waste prevention for hotels using federated learning and blockchain  </h2>

<p align="center">
<h3 align="center"><a href="https://www.youtube.com/watch?v=iYYjNOSB26s&ab_channel=InnovationFactory">View the demo video</a><br>🍴🤝🏩</h3> 

</p>
<p align="center">
<img src="./images/tech_stack.png" height="300" width="640">
</p>
</div>


## Description 📝

This is a prototype for an implementation using Flower federated learning framework and private Ethereum blockchain network to help hotels forecast the food quantities to buy in order to avoid losing money and avoid throwing away food. 

This repository contains the following:

- The code to run the central organisation under /server directory.
- The code to run the client organisation under /client directory.
 
## Features 💡
For the central organization:
- Launching an FL session.
- Consulting clients contributions in FL sessions.

For the client organization:
- Contribute local data to use for the local training.
- Forecast food elements quantities to buy using a global model.



## Architecture 👷‍♂️
The Figure below represents our global application architecture of the solution.
<div align="center">
<img src="./images/architecture.png">
</div>
<div>
The central and the client organizations authenticate to their corresponding accounts. To start an FL session, the central organization launches its flower server script (1) and that triggers an event in the blockchain network to notify all the clients (2). When notified about the start of the training session, the client automatically starts the flower client (4) and imports his local data to the application (5). If the server doesn’t choose to initialize the model parameters from those of the previous FL training session, it requests the initial model parameters from a random client and broadcasts it to the rest of the clients. Once all participants complete the training round, their corresponding model updates get sent to the server where the aggregation of all weights happens and then each client gets a reward proportional to how many local data points he used during the training process (9).  

In each federated session, the server stores the result of the aggregated weights (7) and their corresponding hashes as well as the strategy used during that session (8). The client keeps a history of the weight files he generated in each round during every federated session (7) aside from the global model parameters he receives at the end of each round and which it uses to make a prediction in a further stage (10).  
</div>

## Requirements
To successfully run the project the following must be installed:
- Anaconda Python distribution
- The python virtual environment through the Anaconda prompt using the command:<br/>
`conda env create -f food_waste_prevention_env.yml`
- NodeJs
- Angular CLI <br/>`npm install -g @angular/cli`

## How to use ? 👩‍🏫 
you can manually start the different processes as follows.

1. deploy smart contracts using truffle and copy the build files under back/config/contracts/ for both server and client 
2. for central organization 
    -  Install the node dependencies in the front folder by running:
        `npm install`
    -  Run the front using:
        `ng serve`
    - activate the anaconda environment from the anaconda prompt with the command: 
        `conda activate food_waste_prevention_env `
    - Start the uvicorn server from the anaconda prompt with the command: 
        `uvicorn server_api:app --port port_number`
3. for client organization 
    -  Install the node dependencies in the front folder by running:
        `npm install`
    -  Run the front using:
        `ng serve`
    - activate the anaconda environment from the anaconda prompt with the command: 
        `conda activate food_waste_prevention_env `
    - Start the uvicorn server from the anaconda prompt with the command: 
        `uvicorn client_api:app --port port_number`

## Demo 🚀
A video demonstration of the implementation can be visited through this<a href="https://www.youtube.com/watch?v=iYYjNOSB26s&ab_channel=InnovationFactory"> link</a>


## what's missing ?📌
- implementing secure aggregation

## Licence 🗣
[MIT](https://opensource.org/licenses/MIT)


